package fr.doranco.tpjsf.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class DBManager {

    private static final Logger logger = Logger.getLogger(DBManager.class.getName());

    private String url;
    private String username;
    private String password;

    public DBManager(String url, String username, String password, String driver) {
        this.url = url;
        this.username = username;
        this.password = password;

        try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			logger.severe("error in setup driver " + driver);
		}
    }

    public Connection getConnection() throws SQLException {
        logger.info("Create DB connection");
        return DriverManager.getConnection(this.url,this.username,this.password);
    }
}
