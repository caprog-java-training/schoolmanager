package fr.doranco.tpjsf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.doranco.tpjsf.entity.User;
import fr.doranco.tpjsf.utils.Dates;

public class UserDao implements IUserDao {

	private static final String SAVE_USER = "insert into user(firstname, lastname, gender, birthday, email, service_level, password) values (?, ?, ?, ?, ?, ?, ?)";
	private static final String FIND_ALL_USERS = "select * from user";
	private static final String DELETE_USER = "delete from user where id = ?";
	private static final String UPDATE_USER = "update user set firstname=?, lastname=?, gender=?, birthday=?, email=?, service_level=? where id=?";
	private static final String FIND_USER_BY_EMAIL = "select * from user where email = ?";
	
	private static final int FIRST_KEY_GENERATED = 1;

	private static final int NO_UPDATE_LINES = 0;
	private static final int ONE_UPDATE_LINE = 1;

	private static final int DELETED_ONE_DATA = 1;
	private static final int NOT_DELETED_DATA = 0;

	private static final String FISTNAME_FIELD = "firstname";
	private static final String LASTNAME_FIELD = "lastname";
	private static final String ID_FIELD = "id";
	private static final String EMAIL_FIELD = "email";
	private static final String GENDER_FIELD = "gender";
	private static final String SERVICE_LEVE_FIELD = "service_level";
	private static final String BIRTHDAY_FIELD = "birthday";
	private static final String PASSWORD_FIELD = "password";

	private final DBManager dbManager = MySqlDBManager.getInstance();

	@Override
	public User saveUser(User user) throws Exception {
		try (Connection con = dbManager.getConnection();
				PreparedStatement ps = con.prepareStatement(SAVE_USER, Statement.RETURN_GENERATED_KEYS)) {

			ps.setString(1, user.getFirstname());
			ps.setString(2, user.getLastname());
			ps.setString(3, user.getGender());
			ps.setDate(4, Dates.convertDateToDateSQL(user.getBirthday()));
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getServiceLevel());
			ps.setString(7, user.getPassword());
			

			int nbAddedLines = ps.executeUpdate();

			if (nbAddedLines == NO_UPDATE_LINES)
				throw new SQLException("Erreur: l'utilisateur n'a pas pu �tre ajout�");

			if (nbAddedLines > ONE_UPDATE_LINE)
				throw new SQLException("Erreur: plusieurs lignes ont �t� ajout�s");

			ResultSet rs = ps.getGeneratedKeys();

			if (rs.next())
				user.setId(rs.getInt(FIRST_KEY_GENERATED));

			return user;
		}
	}

	@Override
	public List<User> findAll() throws SQLException {
		try (Connection con = dbManager.getConnection(); 
				PreparedStatement ps = con.prepareStatement(FIND_ALL_USERS)) {

			ResultSet rs = ps.executeQuery();
			List<User> userList = new ArrayList<User>();

			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt(ID_FIELD));
				user.setFirstname(rs.getString(FISTNAME_FIELD));
				user.setLastname(rs.getString(LASTNAME_FIELD));
				user.setEmail(rs.getString(EMAIL_FIELD));
				user.setGender(rs.getString(GENDER_FIELD));
				user.setServiceLevel(rs.getString(SERVICE_LEVE_FIELD));
				user.setBirthday(rs.getDate(BIRTHDAY_FIELD));
				userList.add(user);
			}

			return userList;
		}
	}

	@Override
	public void deleteUser(int id) throws Exception {
		try (Connection con = dbManager.getConnection(); 
				PreparedStatement ps = con.prepareStatement(DELETE_USER)) {

			ps.setInt(1, id);

			int nbLinesUpdated = ps.executeUpdate();

			if (nbLinesUpdated > DELETED_ONE_DATA)
				throw new Exception("Multiple lines was deleted!");

			if (nbLinesUpdated == NOT_DELETED_DATA)
				throw new Exception("Multiple lines was deleted!");
		}
	}

	@Override
	public User updateUser(User user) throws SQLException {
		try (Connection con = dbManager.getConnection();
				PreparedStatement ps = con.prepareStatement(UPDATE_USER)) {

			ps.setString(1, user.getFirstname());
			ps.setString(2, user.getLastname());
			ps.setString(3, user.getGender());
			ps.setDate(4, Dates.convertDateToDateSQL(user.getBirthday()));
			ps.setString(5, user.getEmail());
			ps.setString(6, user.getServiceLevel());
			ps.setInt(7, user.getId());

			int nbAddedLines = ps.executeUpdate();

			if (nbAddedLines == NO_UPDATE_LINES)
				throw new SQLException("Erreur: l'utilisateur n'a pas pu �tre mis � jour");

			if (nbAddedLines > ONE_UPDATE_LINE)
				throw new SQLException("Erreur: plusieurs lignes ont �t� modifi�es");

			return user;
		}
	}

	@Override
	public User findByEmail(String email) throws SQLException {
		try (Connection con = dbManager.getConnection(); 
				PreparedStatement ps = con.prepareStatement(FIND_USER_BY_EMAIL)) {

			ps.setString(1, email);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				User user = new User(); 
				user.setId(rs.getInt(ID_FIELD));
				user.setFirstname(rs.getString(FISTNAME_FIELD));
				user.setLastname(rs.getString(LASTNAME_FIELD));
				user.setEmail(rs.getString(EMAIL_FIELD));
				user.setGender(rs.getString(GENDER_FIELD));
				user.setServiceLevel(rs.getString(SERVICE_LEVE_FIELD));
				user.setBirthday(rs.getDate(BIRTHDAY_FIELD));
				user.setPassword(rs.getString(PASSWORD_FIELD));
				return user;
			}

			return null;
		}
	}

}
