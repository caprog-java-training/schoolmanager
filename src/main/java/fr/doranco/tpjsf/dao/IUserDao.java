package fr.doranco.tpjsf.dao;

import java.sql.SQLException;
import java.util.List;

import fr.doranco.tpjsf.entity.User;

public interface IUserDao {
	User saveUser(User user) throws Exception;

	List<User> findAll() throws SQLException;

	void deleteUser(int id) throws Exception;

	User updateUser(User user) throws SQLException;

	User findByEmail(String email) throws SQLException;
}
