package fr.doranco.tpjsf.dao;

public class MySqlDBManager extends DBManager {

	private static DBManager instance;
	
	public MySqlDBManager(String url, String username, String password, String driver) {
		super(url, username, password, driver);
	}
	
	public synchronized static DBManager getInstance(){
		
		if(instance == null)
			instance = new MySqlDBManager("jdbc:mysql://localhost:3306/schoolmanager", "root", "", "com.mysql.cj.jdbc.Driver");
		
		return instance;
	}

}
