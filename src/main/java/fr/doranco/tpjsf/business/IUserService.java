package fr.doranco.tpjsf.business;

import java.sql.SQLException;
import java.util.List;

import fr.doranco.tpjsf.entity.User;

public interface IUserService {
	
	User saveUser(User user) throws Exception;

	List<User> findAll() throws SQLException;

	void deleteUser(int id) throws Exception;

	User updateUser(User user) throws SQLException;

	User login(String email, String password) throws Exception;
}
