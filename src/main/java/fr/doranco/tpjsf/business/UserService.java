package fr.doranco.tpjsf.business;

import java.sql.SQLException;
import java.util.List;

import fr.doranco.tpjsf.dao.IUserDao;
import fr.doranco.tpjsf.dao.UserDao;
import fr.doranco.tpjsf.entity.User;

public class UserService implements IUserService {

	private IUserDao userDao = new UserDao();
	
	@Override
	public User saveUser(User user) throws Exception {
		
		user.setLastname(user.getLastname().toUpperCase());
		
		String firstLetterUpperCase = user.getFirstname().substring(0, 1).toUpperCase();
		String otherLettersLowerCase = user.getFirstname().substring(1).toLowerCase();
		
		user.setFirstname(firstLetterUpperCase.concat(otherLettersLowerCase));
		
		return userDao.saveUser(user);
	}

	@Override
	public List<User> findAll() throws SQLException {
		return userDao.findAll();
	}

	@Override
	public void deleteUser(int id) throws Exception {
		userDao.deleteUser(id);
	}

	@Override
	public User updateUser(User user) throws SQLException {
		
		return userDao.updateUser(user);
	}

	@Override
	public User login(String email, String password) throws Exception {
		
		System.out.println(password);
		User user = userDao.findByEmail(email);
		System.out.println(user.getPassword());
		// WARNING: Use hash password algorithm bcript or other 
		if(user == null || !user.getPassword().equals(password))
			throw new Exception("Email or password incorrect");
		
		return user;
	}

}
