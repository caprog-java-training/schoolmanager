package fr.doranco.tpjsf.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dates {

	private static final DateFormat DATE_FORMATTER =  new SimpleDateFormat("dd/MM/yyyy");
			
	public static Date converStringToDateUtil(String date) throws ParseException {
		
		return DATE_FORMATTER.parse(date);
	}

	public static java.sql.Date convertDateToDateSQL(Date date) {
		
		return new java.sql.Date(date.getTime());
	}

	public static String convertDateToString(Date date) {
		
		return DATE_FORMATTER.format(date);
	}

}
