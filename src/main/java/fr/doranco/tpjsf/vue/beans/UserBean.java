package fr.doranco.tpjsf.vue.beans;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.doranco.tpjsf.business.IUserService;
import fr.doranco.tpjsf.business.UserService;
import fr.doranco.tpjsf.dao.DBManager;
import fr.doranco.tpjsf.entity.User;
import fr.doranco.tpjsf.utils.Dates;
import fr.doranco.tpjsf.vue.dto.UserDTO;

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean {

	private static final Logger logger = Logger.getLogger(DBManager.class.getName());
	private IUserService userService = new UserService();

	private String lastname;
	private String firstname;
	private String birthday;
	private String gender;
	private String email;
	private String serviceLevel;
	private String password;

	private String successMessage;
	private String errorMessage;

	private List<User> userList;

	private UserDTO selectedUser;

	
	public String login() {
		
		try {
			userService.login(email, password);
			
			cleanBeanProps();
			
			return "users";
		} catch (Exception e) {
			logger.severe("Error to login user: " + e.getMessage());
			errorMessage = "Utilisateur et/ou mot de passe incorrect/s";
		}
		
		return "";
	}


	public String signUp() {

		errorMessage = "";
		successMessage = "";

		User user = buildUser(lastname, firstname, email, serviceLevel, gender, birthday, password);

		try {
			userService.saveUser(user);
			
			cleanBeanProps();
			
			successMessage = "Utilisateur cr�� avec succ�s";
			updateUserList();
		} catch (Exception e) {
			logger.severe("Error to save user:" + e.getMessage());
			errorMessage = "Erreur dans la cr�ation d'utilisateur";
		}

		return "";
	}

	private User buildUser(String lastname, String firstname, String email, String serviceLevel, String gender, String birthday, String password) {
		User user = new User();
		user.setLastname(lastname);
		user.setFirstname(firstname);
		user.setEmail(email);
		user.setServiceLevel(serviceLevel);
		user.setGender(gender);
		user.setPassword(password);

		try {

			user.setBirthday(Dates.converStringToDateUtil(birthday));

		} catch (ParseException e) {
			logger.severe("Error to parse date");
		}
		
		return user;
	}
	
	public String updateUser(UserDTO userDTO) {
		errorMessage = "";
		successMessage = "";
		
		User user = buildUser(userDTO.getLastname(), 
								userDTO.getFirstname(), 
								userDTO.getEmail(), 
								userDTO.getServiceLevel(), 
								userDTO.getGender(), 
								userDTO.getBirthday(),
								null);
		
		user.setId(userDTO.getId());
		
		try {
			userService.updateUser(user);
			updateUserList();
			cleanBeanProps();
			return "users";
		} catch (SQLException e) {
			logger.severe("Error to update user list: " + e.getMessage());
			errorMessage = "Erreur dans la mise � jour de l'utilisateur";
		}
		
		return "";
	}

	public String goUpdateUser(User user) {
		errorMessage = "";
		successMessage = "";
		
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setBirthday(Dates.convertDateToString(user.getBirthday()));
		dto.setEmail(user.getEmail());
		dto.setFirstname(user.getFirstname());
		dto.setGender(user.getGender());
		dto.setLastname(user.getLastname());
		dto.setServiceLevel(user.getServiceLevel());
		
		selectedUser = dto;
		return "update_user";
	}
	
	public String deleteUser(User user) {
		try {
			userService.deleteUser(user.getId());
			updateUserList();
		} catch (Exception e) {
			logger.severe("Error in action delete user: " + e.getMessage());
		}
		return "";
	}
	
	public String formatDate(Date date) {
		return Dates.convertDateToString(date);
	}
	
	public String formatGender(String gender) {
		switch(gender.toLowerCase()) {
			case "man": return "Homme";
			case "woman": return "Femme";
			default : return "Inconnu";
		}
	}
	
	
	private void cleanBeanProps() {
		lastname = null;
		firstname = null;
		birthday = null; 
		gender = null;
		email = null;
		serviceLevel = null;
		password = null;
		selectedUser = null;
	}
	
	private void updateUserList() throws SQLException {
		userList = userService.findAll();
	}

	public boolean hasErrorMessage() {
		return errorMessage != null && errorMessage.length() > 0;
	}

	public boolean hasSuccessMessage() {
		return successMessage != null && successMessage.length() > 0;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getServiceLevel() {
		return serviceLevel;
	}

	public void setServiceLevel(String serviceLevel) {
		this.serviceLevel = serviceLevel;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public synchronized List<User> getUserList() {
		try {
			if (userList == null)
				userList = userService.findAll();
		} catch (Exception e) {
			logger.severe("Error to find all users:" + e.getMessage());
		}
		return userList;
	}

	public UserDTO getSelectedUser() {
		return selectedUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
